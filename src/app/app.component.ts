import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Posts';
  posts = [
    {
      title: 'Mon premier post',
      content: 'Nam sole orto magnitudine ainnare temere contextis cratibuime trucidarunt.',
      loveIts: 0,
      dontLoveIts: 0,
      createdAt: Date()
    },
    {
      title: 'Mon deuxième post',
      content: 'Ut enim benefici liberalesque itiam non spe mercedis adducti sed qu.',
      loveIts: 0,
      dontLoveIts: 0,
      createdAt: Date()
    },
    {
      title: 'Encore un post',
      content: 'Siquis enim militarium vel honoratorum aut nobilis inter suos rurgente.',
      loveIts: 0,
      dontLoveIts: 0,
      createdAt: Date()
    }
  ];
}
