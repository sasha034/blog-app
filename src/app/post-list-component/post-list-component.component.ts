import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit {

  createdAt = new Promise(
    (resolve, reject) => {
      const date = new Date();
      setTimeout(
        () => {
          resolve(date);
        }, 2000
      );
    }
  );

  @Input() title: string;
  @Input() content: string;
  @Input() loveIts: number;
  @Input() dontLoveIts: number;

  constructor() {}

 getColor() {
   if (this.loveIts > this.dontLoveIts) {
     return 'green';
   } else if (this.dontLoveIts > this.loveIts) {
    return 'red';
   } else {
     return 'black';
   }
 }

 getLoveIts() {
    return this.loveIts;
 }

 getDontLoveIts() {
    return this.dontLoveIts;
}

onClickLoveIts() {
    this.loveIts++;
}

onClickDontLoveIts() {
  this.dontLoveIts++;
}

  ngOnInit() {}
}
